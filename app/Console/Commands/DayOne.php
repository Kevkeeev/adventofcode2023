<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class DayOne extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'one {part} {--test}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute day one';
	
	// put 0 first, so we can search by value.
	private static $digits = ['0','1','2','3','4','5','6','7','8','9'];
	private static $strings = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
	
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
		$part = $this->argument('part');
		$filename = '';
		if($this->option('test')) {
			$filename .= '-test-' . $part;
		}
		
		$input = Storage::disk('input')->get('one'. $filename . '.txt');
		$lines = explode("\n", $input);
		
		$search = self::$digits;
		if('two' === $this->argument('part')) {
			$search = array_merge($search, self::$strings);
		}
		
		$total = 0;
		foreach($lines as $line) {
//			dump($line);
			
			$numbers = [];
			foreach($search as $value) {
				$offset = 0;
				while(($index = strpos($line, $value, $offset)) !== FALSE) {
					$numbers[$index] = $value;
					$offset = $index + 1; // search from the next character on.
//					dump($value, strpos($line, $value), $numbers);
				}
			}
			
			if(count($numbers) == 0) {
				continue;
			}
			
			ksort($numbers);
			
			$num1 = $this->numberToInt($numbers[array_key_first($numbers)]);
			$total += 10*$num1;
			
			$num2 = $this->numberToInt($numbers[array_key_last($numbers)]);
			$total += $num2;
			
//			dump($line, $numbers, $num1, $num2);
/*
			$new_string = preg_replace("~[^0-9]~", "", $line);
			
			$num1 = substr($new_string, 0,1);
			$num2 = substr($new_string, -1,1);
			
			$total += (10 * (int)$num1) + (int)$num2;
*/
		}
		
		$this->output->info($total);
		
        return Command::SUCCESS;
    }
	
	private function numberToInt(string $number) : int
	{
		if(is_numeric($number)) {
			return (int)$number;
		}
		
		if(in_array($number, self::$strings)){
			return array_search($number, self::$strings);
		}
		
		throw new \InvalidArgumentException();
	}
	
}
